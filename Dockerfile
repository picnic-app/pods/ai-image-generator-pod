# syntax=docker/dockerfile:1

ARG NODE_VERSION=19.7
ARG ALPINE_VERSION=3.16

FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION} AS build

WORKDIR /app

RUN npm install -g @angular/cli

COPY package.json package-lock.json /app
RUN npm install

COPY . /app
RUN ng build --configuration=development --base-href=/ai-image-generator/
RUN ng build --configuration=production --base-href=/ai-image-generator/

FROM fabiocicerchia/nginx-lua:1.23.3-alpine AS nginx

COPY --from=build /app/dist /usr/share/nginx/html

COPY .cfg/nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]

