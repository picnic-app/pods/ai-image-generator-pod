import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FinishComponent } from './finish.component';

describe('FinishComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    declarations: [FinishComponent]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(FinishComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
