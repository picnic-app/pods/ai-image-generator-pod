import { POST, BasePath, Header, Body, BaseService, ServiceBuilder, Response, Multipart, Part, PartDescriptor, RequestInterceptorFunction, PUT, Path } from "ts-retrofit";

export interface GenerativeImageRequest {
    style: string;
    prompt: string;
}

export interface GenerativeImageHTTPResponse {
    image_url: string;
}

export enum UploadType {
    circle = 'circle',
    content = 'content',
}

export interface UploadFileRequest {
  uploadType: string;
}

export interface UploadFileHTTPResponse {
    file_id: string;
}

export interface CreatePostInput {
    circleId: string;
    type: string;
    content: CreatePostImageContent;
}

export interface CreatePostImageContent {
    id: string;
    caption: string;
}

export interface CreatePostResponse {
    postId: string;
}

export interface UpdateCircleCoverImageRequest {
    fileId: string;
}

export interface UpdateCircleImageRequest {
    fileId: string;
}

@BasePath("/api")
class OpenApiService extends BaseService {
    @POST("/ai/generative/image")
    async getGenerativeImage(
        @Header("Authorization") authorization: string,
        @Body request: GenerativeImageRequest,
    ): Promise<Response<GenerativeImageHTTPResponse>> {
        return <Response<GenerativeImageHTTPResponse>>{}
    };

    @POST("/upload")
    @Multipart
    async upload(
        @Header("Authorization") authorization: string,
        @Part("file") file: PartDescriptor<Blob>,
        @Body request: UploadFileRequest,
    ): Promise<Response<UploadFileHTTPResponse>> {
        return <Response<UploadFileHTTPResponse>>{}
    };

    @PUT("/circles/{circleId}/cover-image")
    async uploadCircleCover(
        @Header("Authorization") authorization: string,
        @Path("circleId") circleId: string,
        @Body updateCircleCoverImageRequest: UpdateCircleCoverImageRequest,
    ): Promise<Response<void>> {
        return <Response<void>>{}
    };

    @PUT("/circles/{circleId}/image")
    async uploadCircleImage(
        @Header("Authorization") authorization: string,
        @Path("circleId") circleId: string,
        @Body updateCircleImageRequest: UpdateCircleImageRequest,
    ): Promise<Response<void>> {
        return <Response<void>>{}
    };

    @POST("/posts")
    async createPost(
        @Header("Authorization") authorization: string,
        @Body request: CreatePostInput,
    ): Promise<Response<CreatePostResponse>> {
        return <Response<CreatePostResponse>>{}
    };
}

const RequestInterceptor: RequestInterceptorFunction = (config) => {
    config.withCredentials = false;
    return config;
};

export const apiHost = "https://api.picnicgcp.net";

export const openApiService = new ServiceBuilder()
    .setEndpoint(apiHost)
    .setRequestInterceptors(RequestInterceptor)
    .build(OpenApiService);
