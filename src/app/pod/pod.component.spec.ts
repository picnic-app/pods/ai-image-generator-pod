import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PodComponent } from './pod.component';

describe('PodComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    declarations: [PodComponent]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(PodComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ai-image-generator-pod'`, () => {
    const fixture = TestBed.createComponent(PodComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ai-image-generator-pod');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(PodComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('ai-image-generator-pod app is running!');
  });
});
