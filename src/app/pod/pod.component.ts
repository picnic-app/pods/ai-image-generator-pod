import { Component, OnInit } from '@angular/core';

import { animate, style, transition, trigger } from "@angular/animations";
import { ActivatedRoute, Router } from '@angular/router';
import { ImageGeneratorService, ImageItem } from '../services/image-generator.service';
import { Style, defaultStyles } from '../domain/style';
import { PostService } from '../services/post.service';
import * as UploadTypesConstants from '../domain/upload_types';
import { CircleService } from '../services/circle.service';
import {ParamsProviderService} from "../services/params-provider.service";

@Component({
    selector: 'pod',
    templateUrl: './pod.component.html',
    styleUrls: ['./pod.component.css'],
    animations: [
        trigger('fade', [
            transition('void => *', [
                style({ opacity: 0 }),
                animate(2000, style({ opacity: 1 }))
            ])
        ])
    ]
})
export class PodComponent implements OnInit {
    title = 'ai-image-generator-pod';
    uploadCircleProfileImage = 'upload to circle image';
    uploadCircleCoverImage = 'upload to circle cover';

    // this variable is used to show hide buttons based on step
    hasGenerated = false;
    imageItem?: ImageItem;
    isLoading = false;
    isLoadingPicnicAction = false;
    prompt = 'some nice image';
    caption = '';
    isPicnicActionError = false;
    isPicnicActionFinished = false;
    buttonLabel = '';
    showStyleSelector = false;
    selectedStyle = 'watercolor';
    styles: Style[] = defaultStyles;
    uploadType = '';



    constructor(
        private imageGeneratorService: ImageGeneratorService,
        private postService: PostService,
        private circleService: CircleService,
        private activatedRoute: ActivatedRoute,
        private paramsProviderService: ParamsProviderService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        const uploadType = this.activatedRoute.snapshot.params["uploadtype"]
        if (uploadType) {
            this.uploadType = uploadType;
            this.showStyleSelector = true;
            if (uploadType == UploadTypesConstants.uploadImage) {
                this.buttonLabel = this.uploadCircleProfileImage;
            } else if (uploadType == UploadTypesConstants.uploadCover) {
                this.buttonLabel = this.uploadCircleCoverImage;
            }
        } else {
            this.buttonLabel = 'post';
            const styleName = this.activatedRoute.snapshot.params["stylename"]
            if (styleName && defaultStyles.find(style => style.name === styleName)) {
                this.selectedStyle = styleName;
                this.showStyleSelector = false;
            } else {
                this.showStyleSelector = true;
            }
        }
    }


    onTapGenerate() {
        this.imageItem = undefined;
        this.hasGenerated = false;
        this.isLoading = true;
        this.imageGeneratorService
            .generateImage({ style: this.selectedStyle, prompt: this.prompt })
            .subscribe({
                next: (imageItem) => {
                    this.imageItem = imageItem;
                    this.isLoading = false;
                    this.hasGenerated = true;
                },
                error: (error) => {
                    console.log('Error generating image:', error);
                    this.isLoading = false;
                    this.imageItem = undefined;
                    this.hasGenerated = false;
                }
            });
    }

    onTapPost() {

        if (!this.imageItem) {
            return;
        }
        this.isPicnicActionError = false;

        this.isLoadingPicnicAction = true;
        this.postService
            .createPost({
                imageUrl: this.imageItem.url,
                caption: this.caption,
            })
            .subscribe({
                next: (post) => {
                    this.isLoading = false;
                    this.isLoadingPicnicAction = false;
                    this.isPicnicActionFinished = true;
                },
                error: (error) => {
                    console.log('Error creating post:', error);
                    this.isLoadingPicnicAction = false;
                    this.isPicnicActionError = true;
                }
            })
    }

    onTapUploadCircleImage() {
        if (!this.imageItem) {
            return;
        }
        this.isPicnicActionError = false;

        this.isLoadingPicnicAction = true;

        var uploadMethod = this.uploadType == UploadTypesConstants.uploadImage ?
            this.circleService.uploadCircleImage(this.imageItem.url) :
            this.circleService.uploadCircleCover(this.imageItem.url);

        uploadMethod
            .subscribe({
                next: (_) => {
                    this.isLoading = false;
                    this.isLoadingPicnicAction = false;
                    this.isPicnicActionFinished = true;
                },
                error: (error) => {
                    console.log('Error uploading image to circle:', error);
                    this.isLoadingPicnicAction = false;
                    this.isPicnicActionError = true;
                }
            })
    }

    onTapSelectStyle(style: Style) {
        this.selectedStyle = style.name;

    }

    onTapRegenerate() {
        this.imageItem = undefined;
        this.onTapGenerate();
    }

    get noCircleSelected() {
      return !this.paramsProviderService.circleId
    }
}
