import { Injectable } from '@angular/core';
import { Observable, mergeMap, from, map } from 'rxjs';
import { UploadType, openApiService } from '../data/openapi.service';
import { ParamsProviderService } from './params-provider.service';
import { UploadService } from './upload.service';

@Injectable({
  providedIn: 'root'
})
export class CircleService {

  constructor(private uploadService: UploadService, private paramsProviderService: ParamsProviderService) { }

  uploadCircleCover(imageUrl: string): Observable<boolean> {
    return this.uploadService
      .uploadImage(imageUrl, UploadType.circle)
      .pipe(
        mergeMap((response) => {
          return from(openApiService.uploadCircleCover(
            this.paramsProviderService.token,
            this.paramsProviderService.circleId,
            {
              fileId: response.fileId,
            },
          ));
        }),
        map((_) => {
          return true;
        }),
      );
  }

  uploadCircleImage(imageUrl: string): Observable<boolean> {
    return this.uploadService
      .uploadImage(imageUrl, UploadType.circle)
      .pipe(
        mergeMap((response) => {
          return from(openApiService.uploadCircleImage(
            this.paramsProviderService.token,
            this.paramsProviderService.circleId,
            {
              fileId: response.fileId,
            },
          ));
        }),
        map((_) => {
          return true;
        }),
      );
  }
}

export interface CreatePostInput {
  imageUrl: string;
}

export interface CreatePostResult {
  postId: string;
}

