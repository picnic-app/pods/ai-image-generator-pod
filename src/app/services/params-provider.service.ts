import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ParamsProviderService {

  token: string = "";
  circleId: string = "";

  constructor() {
    const search = location.search;
    const params = new URLSearchParams(search);
    const encodedToken = params.get('token') ?? "";
    this.token = decodeURIComponent(encodedToken);
    const encodedCircleId = params.get('circleId') ?? "";
    this.circleId = decodeURIComponent(encodedCircleId);
  }
}
