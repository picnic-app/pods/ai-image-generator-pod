import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { UploadType, apiHost } from '../data/openapi.service';
import { HttpClient } from '@angular/common/http';
import { ParamsProviderService } from './params-provider.service';

@Injectable({
  providedIn: 'root',
})
export class UploadService {

  constructor(private http: HttpClient, private paramsProviderService: ParamsProviderService) { }

  uploadImage(url: string, uploadType: UploadType): Observable<UploadFileResult> {
    return this.http
      .get(url, {
        responseType: 'blob',
        withCredentials: false,
      })
      .pipe(
        mergeMap((image) => {

          const formData = new FormData()
          formData.append("file", image);
          formData.append("uploadType", uploadType);

          return this.http.post(`${apiHost}/api/upload`, formData, {
            headers: {
              "Authorization": this.paramsProviderService.token,
            }
          })
        }),
        map((response) => {
          return {
            // @ts-ignore
            fileId: response.file_id,
          };
        }),
      );
  }
}

export interface UploadFileResult {
  fileId: string;
}

