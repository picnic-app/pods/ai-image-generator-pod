import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { openApiService } from '../data/openapi.service';
import { ParamsProviderService } from './params-provider.service';

@Injectable({
  providedIn: 'root',
})
export class ImageGeneratorService {

  constructor(private paramsProviderService: ParamsProviderService) { }

  generateImage(input: GenerateImageInput): Observable<ImageItem> {
    return new Observable<ImageItem>((observer) => {
      openApiService.getGenerativeImage(
        this.paramsProviderService.token,
        {
          style: input.style,
          prompt: input.prompt,
        })
        .then((response) => {
          observer.next({ url: response.data.image_url });
          observer.complete();
        }).catch((error) => {
          console.log(error);
          observer.error(error);
        });
        
      return () => { };
    });
  }
}

interface GenerateImageInput {
  style: string;
  prompt: string;
}

export interface ImageItem {
  url: string;
}
