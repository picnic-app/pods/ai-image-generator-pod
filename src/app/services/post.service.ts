import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { UploadType, openApiService } from '../data/openapi.service';
import { UploadService } from './upload.service';
import { ParamsProviderService } from './params-provider.service';

@Injectable({
  providedIn: 'root',
})
export class PostService {

  constructor(private uploadService: UploadService, private paramsProviderService: ParamsProviderService) { }

  createPost(input: CreatePostInput): Observable<CreatePostResult> {
    return this.uploadService
      .uploadImage(input.imageUrl, UploadType.content)
      .pipe(
        mergeMap((response) => {
          return from(openApiService.createPost(
            this.paramsProviderService.token,
            {
              circleId: this.paramsProviderService.circleId,
              type: "image",
              content: {
                id: response.fileId,
                caption: input.caption,
              }
            },
          ));
        }),
        map((response) => {
          return {
            postId: response.data.postId,
          };
        }),
      );
  }
}

export interface CreatePostInput {
  imageUrl: string;
  caption: string;
}

export interface CreatePostResult {
  postId: string;
}

