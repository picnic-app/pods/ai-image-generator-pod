import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PodComponent } from './pod/pod.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FinishComponent } from './finish/finish.component';

const routes: Routes = [
  { path: 'style/:stylename', component: PodComponent },
  { path: 'circle/:uploadtype', component: PodComponent },
  { path: 'finish', component: FinishComponent },
  { path: '**', component: PodComponent },
];

@NgModule({
  declarations: [PodComponent],
  imports: [RouterModule.forRoot(routes), CommonModule, FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
