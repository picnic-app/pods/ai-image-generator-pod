export interface Style {
    src: string;
    name: string;
}

export const defaultStyles: Style[] = [
    {
        src: 'assets/img/ic_style_watercolor.jpeg',
        name: 'watercolor',
    },
    {
        src: 'assets/img/ic_style_cartoon.jpeg',
        name: 'cartoon',
    },
    {
        src: 'assets/img/ic_style_makeup.jpeg',
        name: 'makeup',
    },
    {
        src: 'assets/img/ic_style_3d.jpeg',
        name: '3D',
    },
    {
        src: 'assets/img/ic_style_oil_painting.jpeg',
        name: 'oil painting',
    },
    {
        src: 'assets/img/ic_style_floral.jpeg',
        name: 'floral',
    },
    {
        src: 'assets/img/ic_style_ink.jpeg',
        name: 'sketch',
    },
    {
        src: 'assets/img/ic_style_anime.jpeg',
        name: 'anime',
    },
];